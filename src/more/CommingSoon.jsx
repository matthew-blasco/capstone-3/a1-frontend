import React from 'react'
import { useSelector } from 'react-redux';
import "./CommingSoon.css";
import BottomTab from './BottomTab';
import Loading from './Loader';
import MetaData from './Metadata';

const CommingSoon = () => {

    const {loading} = useSelector(
        (state) => state.cart
      );

    return (
        <>
        {loading ? (<Loading/>) : (
            <>
        <MetaData title="coming soon" />
            <div>
            <div className='bg'>
                <span dataText="Coming" className='first'>Coming<span dataText="Soon....">Soon....</span></span>
                <div className="one">
                    <div className="circle">

                    </div>
                </div>
            </div>
            </div>
            <BottomTab />
        </>
        )}
        </>
    )
}

export default CommingSoon
