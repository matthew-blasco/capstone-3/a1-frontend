import React from 'react'
import "./Rules.css";
import Header from '../component/Home/Header';
import BottomTab from './BottomTab';
import MetaData from './Metadata';
import Footer from '../Footer';

const Rules = () => {
    return (
        <>
        <MetaData title="Rules" />
        <Header />
        <div className='rules' style={{
            padding:"50px 30px",
            display:"flex",
            width:"95%",
            overflow:"hidden"
        }}>
            <ul className='rules'>
                <span style={{
                    color:"#000",
                    fontSize:"1.3rem",
                    fontWeight:"800",
                    fontFamily:"Roboto",
                }}>Some Rules:</span>
                <li>1. Return of product is possible; however, delivery charge is non-refundable.</li>
                <li>2. Procedure would be: delivery charge payment first before transaction process. </li>
                <li>3. Out-of-stock products cannot be bought.</li>
                <li>4. You can buy any products from us as we are trying our best to give the best quality of products.</li>
                <li>5. You can find more new features in our business very soon. Our developers' team always work for your good services.</li>
                <li>6. Lastly, thanks for visiting our website. Have a good day!</li>
            </ul>
        </div>
        <Footer />
        <BottomTab />
        </>
    )
}

export default Rules
